module gitlab.com/xdevs23/go-httpfs

go 1.17

require (
	bazil.org/fuse v0.0.0-20200524192727-fb710f7dfd05
	github.com/andybalholm/cascadia v1.3.1
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20211109214657-ef0fda0de508
)

require golang.org/x/sys v0.0.0-20211110154304-99a53858aa08 // indirect
